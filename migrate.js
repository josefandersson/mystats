const Sqlite3 = require('sqlite3').verbose();
const path = require('path');

const sqlite3 = new Sqlite3.Database(path.join(__dirname, 'database.sqlite'));
const db = require('./src/database');

(async () => {
    const user = await db.User.findOne({ where:{ username:'josef' }});

    sqlite3.all('SELECT * FROM activity WHERE user_id = 1', (err, rows) => {
        if (err) throw err;
        else {
            rows.forEach(row => {
                db.Activity.findOrCreate({
                    where: { title:row.title },
                    defaults: { title:row.title, type:row.type, public:row.public===1, createdAt:new Date(row.created), userId:user.id }
                });
            });
        }
    });

    setTimeout(() => {
        sqlite3.all('SELECT * FROM entry WHERE activity_id < 8', (err, rows) => {
            if (err) throw err;
            else {
                rows.forEach(row => {
                    db.Entry.create({
                        started: new Date(row.timestamp),
                        length: row.length,
                        public: row.public===1,
                        comment: row.comment,
                        createdAt: new Date(row.created),
                        activityId: row.activity_id
                    });
                });
            }
        });
    }, 5000);
})();