const router = require('express').Router()
const { QueryTypes } = require('sequelize');

const db = require('../src/database');

// user info and list of activities
router.get('/user/:uid', async (req, res) => {
    const uid = req.params.uid;
    const user = await db.User.findOne({ where:{ uid }});
    if (!user) return res.json({ success:false, error:'no user with id', uid });

    const isOwner = req.user && req.user.id === user.id;

    const activities = await db.sequelize.query(
`SELECT
    activities.uid AS uid,
    activities.title AS title,
    activities.type AS type,
    activities.public AS public,
    entries.uid AS 'entry.uid',
    entries.started AS 'entry.started',
    entries.length AS 'entry.length',
    entries.comment AS 'entry.comment',
    (SELECT count(*) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.count',
    (SELECT avg(entries.length) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.average'
FROM activities
LEFT JOIN entries ON entries.id = (SELECT entries.id FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id ORDER BY entries.started DESC LIMIT 1)
WHERE activities.deletedAt IS NULL AND activities.userId = ? ${isOwner ? '' : 'AND activities.public = 1'}
ORDER BY entries.started DESC`, {
        type: QueryTypes.SELECT,
        replacements: [user.id],
        nest: true
    });
    res.json({
        success:true,
        activities,
        user: { displayname:user.displayname },
        isOwner
    });
})

// activity info and list of entries
router.get('/activity/:id', async (req, res) => {
    if (!requires(['id'], req.params, res)) return
    
    const id = req.params.id
    const activity = await (db.Activity.findOne({ where:{ id }}))
    if (!activity) return res.json({ success:false, error:'no activity with id' })

    if ((req.user && req.user.id !== activity.userId) && !user.public)
        return res.json({ success:false, error:'activity is private' })

    const entries = (await activity.getEntries({
        where: req.user.id === activity.userId ? {} : user.public ? { public:1 } : { id:'0' },
    }))

    return res.json({ success:true, activity:{

        entries
    } })
})

// entry info
router.get('/entry/:entry_id')



// edit user (displayname)
router.post('/user/edit')



// add activity
router.post('/activity/add', async (req, res) => {
    if (!requiresLogin(req, res)) return
    if (!requires(['title'], req.body, res)) return

    const build = { title:req.body.title, userId:req.user.id, public:req.user.public }
    if (req.body.type) build.type = req.body.type
    if (req.body.public) build.public = true

    try {
        const activity = await db.Activity.create(build)
        res.json({ success:true, activity })
    } catch (error) {
        console.log('Could not api add activity', error)
        res.json({ success:false, error:'database error' })
    }
})

// edit activity
router.post('/activity/:activity_id/edit', (req, res) => {
    if (requiresLogin(req, res)) {
        if (requires(['activity_id'], req.params, res) && requires(['title', 'type'], req.body, res)) {
            console.log('PUBLIC:', req.body.public || 0)
            db.updateActivity(req.params.activity_id, req.body.title, req.body.type, req.body.public ? 1 : 0)
                .then(() => res.json({ success:true }))
                .catch(e => res.json({ success:false, error:'database error' }))
        }
    }
})

// remove activity
router.post('/activity/:activity_id/remove', (req, res) => {
    if (requiresLogin(req, res)) {
        if (requires(['activity_id'], req.body, res)) {
            db.getActivity(req.body.activity_id).then(activity => {
                if (!activity) return res.json({ success:false, error:'activity does not exist' })
                if (req.user.id !== activity.user_id) return res.json({ success:false, error:'user not owner' })
                db.removeActivity(activity.id)
                    .then(() => res.json({ success:true }))
                    .catch(e => res.json({ success:false, error:'database error' }))
            })
        }
    }
})



// add entry
router.post('/entry/add', async (req, res) => {
    if (requiresLogin(req, res)) {
        if (requires(['uid'], req.body, res)) {
            let uid = req.body.uid,
                date = req.body.date,
                time = req.body.time,
                millis = req.body.millis,
                hours = req.body.hours,
                minutes = req.body.minutes,
                seconds = req.body.seconds,
                public = !!req.body.public,
                comment = req.body.comment;
            let length = 0, timestamp;
            if (hours)   { hours   = parseInt(hours);   length += hours * 3600000 }
            if (minutes) { minutes = parseInt(minutes); length += minutes * 60000 }
            if (seconds) { seconds = parseInt(seconds); length += seconds * 1000  }
            if (millis)            timestamp = parseInt(millis);
            else if (date && time) timestamp = Date.parse(`${date} ${time}`);
            else                   timestamp = Date.now();
            const activity = await db.Activity.findOne({ where:{ uid } });
            if (!activity) return res.json({ error:'unknown activity', uid });
            if (req.user.id !== activity.userId) return res.json({ error:'user not owner' });
            const entry = await db.Entry.create({
                started: timestamp,
                length,
                public,
                comment,
                activityId: activity.id
            });
            res.json({ success:true, entry });
        }
    }
})

// edit entry
router.post('/entry/:uid/edit', async (req, res) => {
    if (requiresLogin(req, res)) {
        if (requires(['uid'], req.params, res)) {
            const uid = req.params.uid;
            const entry = await db.Entry.findOne({
                where: { uid },
                include: [{
                    model: db.Activity,
                    attributes: ['userId']
                }]
            });
            if (!entry) return res.json({ error:'unknown entry', uid });
            if (entry.activity.userId !== req.user.id) return res.json({ error:'user not owner' });

            const comment = req.body.comment;
            const oldComment = entry.comment;
            if (comment) {
                entry.comment = comment;
            }

            await entry.save();
            res.json({ success:true, uid, oldComment, comment });
        }
    }
});

// remove entry
router.post('/entry/:uid/remove', async (req, res) => {
    if (requiresLogin(req, res)) {
        if (requires(['uid'], req.params, res)) {
            const uid = req.params.uid;
            const entry = await db.Entry.findOne({
                where: { uid },
                include: [{
                    model: db.Activity,
                    attributes: ['userId']
                }]
            });
            if (!entry) return res.json({ error:'unknown entry', uid });
            if (entry.activity.userId !== req.user.id) return res.json({ error:'user not owner' });
            await entry.destroy();
            res.json({ success:true });
        }
    }
});


function requiresLogin(req, res) {
    if (req.user) return true
    
    res.json({ success:false, error:'not logged in' })
    return false
}

function requires(needed, body, res) {
    if (!needed.find(e => !body[e])) return true
    
    res.json({ success:false, error:'bad request', body })
    return false
}


module.exports = router