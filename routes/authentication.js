const router = require('express').Router();
const crypto = require('crypto');

const auth = require('../src/authentication');
const db = require('../src/database');

const config = require('../config');

router.use(async (req, res, next) => {
    let sid;
    const ipRes = await db.IpAddress.findOrCreate({ where:{ ip:req.ipInfo.ip }, defaults:{ ip:req.ipInfo.ip, country:req.ipInfo.country } });
    const ipAddress = ipRes[0];
    if (ipAddress.banned) {
        res.send('Your ip is banned from using this website.');
        return;
    }
    if (!(req.cookies && req.cookies.sid) || req.cookies.sid.length !== 88) {
        sid = crypto.randomBytes(64).toString('base64');
        req.user = null;
        req.cookies.sid = sid;
        res.cookie('sid', sid, { maxAge:60 * 60 * 24 * 7 * 1000, sameSite:'strict', secure:true });
    } else {
        sid = req.cookies.sid;
        const user = await auth.postponeExpires(sid);
        if (user) {
            if (user.lastIpId !== ipAddress.id) {
                user.lastIpId = ipAddress.id;
                await user.save();
            }
            if (user.countryLock && user.country !== req.ipInfo.country) {
                await auth.logout(user, sid);
            } else {
                req.user = user;
            }
            res.cookie('sid', sid, { maxAge:config.APP.SESSION_INACTIVE_TIME, sameSite:'strict', secure:true });
        } else {
            res.cookie('sid', sid, { maxAge:9000000000, sameSite:'strict', secure:true });
        }
    }
    next();
});

router.get('/login', (req, res) => {
    if (req.user) res.redirect('/');
    else res.render('login', { register:false });
});

router.get('/register', (req, res) => {
    if (req.user) res.redirect('/');
    else res.render('login', { register:true });
});

router.get('/logout', async (req, res) => {
    try {
        await auth.logout(req.user, req.cookies.sid);
        res.redirect('/');
    } catch (error) {
        console.log('Could not logout:', error);
        res.redirect('/?e=database error');
    }
});

router.post('/login', async (req, res) => {
    if (req.user) return res.redirect('/');
    
    try {
        const user = await auth.login(req.body.username, req.body.password, req.cookies.sid, req.ipInfo.ip);
        res.redirect('/');
    } catch (error) {
        if (error === 'unknown user'
            || error === 'wrong password'
            || error === 'bad session id'
            || error === 'too many login attempts')
                res.redirect(`/login?e=${error}`);
        else {
            console.log('could not login:', error);
            res.redirect('/login?e=database error');
        }
    }
});

router.post('/register', async (req, res) => {
    if (req.user) return res.redirect('/');
    if (req.body.password !== req.body.cpassword) return res.redirect('/register?e=passwords do not match');
    
    const lenOffset = auth.checkPwdLength(req.body.password);
    if (lenOffset < 0) return res.redirect('/register?e=password too short');
    if (lenOffset > 0) return res.redirect('/register?e=password too long');

    if (await auth.register(req.body.username, req.body.password, req.body.email, req.cookies.sid, req.ipInfo.ip, req.ipInfo.country)) {
        res.redirect('/');
    } else {
        res.redirect('/register?e=already exists or bad input');
    }
});

router.post('/api/login', async (req, res) => {
    if (req.user) return res.json({ error:'already logged in' });
    
    try {
        const user = await auth.login(req.body.username, req.body.password, req.cookies.sid, req.ipInfo.ip);
        res.json({ success:true });
    } catch (error) {
        if (error === 'unknown user'
            || error === 'wrong password'
            || error === 'bad session id'
            || error === 'too many login attempts')
                res.json({ error });
        else {
            console.log('could not login:', error);
            res.json({ error:'database error' });
        }
    }
});

router.post('/api/register', async (req, res) => {
    if (req.user) return res.json({ error:'already logged in' });
    if (req.body.password !== req.body.cpassword) return res.json({ error:'passwords do not match' });

    const lenOffset = auth.checkPwdLength(req.body.password);
    if (lenOffset < 0) return res.json({ error:'password too short' });
    if (lenOffset > 0) return res.json({ error:'password too long' });
    console.log(req.ipInfo);
    try {
        const user = await auth.register(req.body.username, req.body.password, req.body.email, req.cookies.sid, req.ipInfo.ip, req.ipInfo.country);
        res.json({ success:true });
    } catch (error) {
        res.json({ error });
    }
});

router.post('/api/logout', async (req, res) => {
    try {
        await auth.logout(req.user, req.cookies.sid);
        res.json({ success:true });
    } catch (error) {
        if (error === 'bad session id' || error === 'not logged in')
            res.json({ error });
        else
            res.json({ error:'database error' });
    }
});

router.post('/api/logout_all', async (req, res) => {
    try {
        await auth.logoutAll(req.user);
        res.json({ success:true });
    } catch (error) {
        if (error === 'not logged in')
            res.json({ error });
        else
            res.json({ error:'database error' });
    }
});


module.exports = router;