const express      = require('express');
const path         = require('path');
const logger       = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const expressip    = require('express-ip');
const http         = require('http');
const https        = require('https');
const fs           = require('fs');
const db           = require('./src/database_sequelize');
const { QueryTypes } = require('sequelize');

const config = require('./config');

const app = express();

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressip().getIpInfoMiddleware);
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./routes/authentication'));
app.use('/api', require('./routes/api'));

app.get('/', async (req, res) => {
    if (!req.user) return res.render('login');

    const activities = await db.sequelize.query(
`SELECT
    activities.uid AS uid,
    activities.title AS title,
    activities.type AS type,
    activities.public AS public,
    entries.uid AS 'entry.uid',
    entries.started AS 'entry.started',
    entries.length AS 'entry.length',
    entries.comment AS 'entry.comment',
    (SELECT count(*) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.count',
    (SELECT avg(entries.length) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.average'
FROM activities
LEFT JOIN entries ON entries.id = (SELECT entries.id FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id ORDER BY entries.started DESC LIMIT 1)
WHERE activities.deletedAt IS NULL AND activities.userId = ?
ORDER BY entries.started DESC`, {
        type: QueryTypes.SELECT,
        replacements: [req.user.id],
        nest: true
    });
    res.render('activities', { json:{
        activities
    }, title:'Your activities', user:req.user.forBrowser(), isOwner:true });
});

app.get('/user/:uid', async (req, res) => {
    const uid = req.params.uid;
    if (!uid) return res.render('error', { title:'Error', user:req.user, message:'No uid.' });

    const user = await db.User.findOne({ where:{ uid }});
    if (!user) return res.render('error', { title:'Error', user:req.user, message:`No user with uid ${uid}.` });

    const isOwner = req.user && req.user.id === user.id;

    const activities = await db.sequelize.query(
`SELECT
    activities.uid AS uid,
    activities.title AS title,
    activities.type AS type,
    activities.public AS public,
    entries.uid AS 'entry.uid',
    entries.started AS 'entry.started',
    entries.length AS 'entry.length',
    entries.comment AS 'entry.comment',
    (SELECT count(*) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.count',
    (SELECT avg(entries.length) FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id) AS 'entries.average'
FROM activities
LEFT JOIN entries ON entries.id = (SELECT entries.id FROM entries WHERE entries.deletedAt IS NULL AND entries.activityId = activities.id ORDER BY entries.started DESC LIMIT 1)
WHERE activities.deletedAt IS NULL AND activities.userId = ? ${isOwner ? '' : 'AND activities.public = 1'}
ORDER BY entries.started DESC`, {
        type: QueryTypes.SELECT,
        replacements: [user.id],
        nest: true
    });
    res.render('activities', { json:{
        activities
    }, title:`${user.displayname}'s activities`, user:req.user, isOwner });
});

app.get('/activity/:uid', async (req, res) => {
    const uid = req.params.uid;
    if (!uid) return res.render('error', { title:'Error', user:req.user, message:'No uid.' });

    const activity = await db.Activity.findOne({
        where: { uid },
        include: [{
            model:db.Entry,
            attributes:[
                'uid',
                'started',
                'length',
                'public',
                'comment'
            ]
        }],
        order:[[db.Entry, 'started','desc']],
    });

    if (!activity) return res.render('error', { title:'Error', user:req.user, message:`No activity with uid "${uid}".` });

    const clientUid = req.user ? req.user.id : -1;
    if (!activity.public && !(clientUid === activity.userId)) return res.render('error', { title:'Error', user:req.user, message:`Activity is not public.` });

    res.render('activity', { json:{
        activity
    }, title:`Entries for ${activity.title}`, user:req.user, isOwner:clientUid===activity.userId });
});


app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send(err.message);
});

const httpServer = http.createServer(app);
httpServer.listen(config.APP.HTTP_PORT);

process.on('uncaughtException', err => console.log(err));







function lengthString(length) {
    if (length == null || !isFinite(length)) return ''
    if (length == 0) return 'Instant'
    return 'Took ' + intervalString(length)
}

function countString(count) {
    if (count === 0) return 'no entries'
    if (count === 1) return '1 entry'
    return `${count} entries`
}

function colorDegree(activity, ms=604800000) { // one week in ms
    if (activity.activity_type === 0 || activity.entry_timestamp == null || !isFinite(activity.entry_timestamp)) {
        return null
    } else if (activity.activity_type === 1) { // pos
        return 120 - (Math.min((Date.now() - activity.entry_timestamp) / ms, 1) * 120)
    } else if (activity.activity_type === 2) { // neg
        return Math.min((Date.now() - activity.entry_timestamp) / ms, 1) * 120
    }
}

function averageBetweenString(averageLength, numEntries, created) {
    if (!numEntries || numEntries === 0) return null
    let deadTime = averageLength * numEntries
    if (isFinite(deadTime)) return intervalString((Date.now() - created + deadTime) / numEntries)
    else return intervalString((Date.now() - created) / numEntries)
}