const { TextEncoder } = require('util');
const { Op } = require('sequelize');

const db = require('../src/database');

const config = require('../config');

function checkPwdLength(pw) {
    const len = new TextEncoder().encode(pw).length;
    if (len < config.APP.PW_MIN_BYTES)
        return len - config.APP.PW_MIN_BYTES;
    if (len > config.APP.PW_MAX_BYTES)
        return len - config.APP.PW_MAX_BYTES;
    return 0;
}

async function postponeExpires(sessionId) {
    try {
        const session = await db.Session.findOne({ where:{ id:sessionId }, include:db.User });
        session.postponeExpires();
        return session.user;
    } catch (error) {}
    return null;
}

async function register(username, password, email, sessionId, ip, country='--') {
    if (!sessionId || sessionId.length !== 88) throw 'bad session id';

    try {
        const ipAdr = await db.IpAddress.findOrCreate({ where:{ ip }, defaults:{ ip, country }});
        const ipId = ipAdr[0].dataValues.id || ipAdr[0].id;
        console.log(ipId);
        const user = await db.User.create({ username, password, email, registrationIpId:ipId, lastIpId:ipId, country, sessions:[{ id:sessionId }] }, { include:db.Session });
        return user;
    } catch (error) {
        console.log(error);
        if (error.name === 'SequelizeUniqueConstraintError')
            throw 'user already exists';
        if (error.name === 'SequelizeValidationError')
            throw 'bad email';
        console.log('Could not register:', error);
        throw 'database error';
    }
}

async function login(username, password, sessionId, ip) {
    if (sessionId.length !== 88) throw 'bad session id';
    
    const createdAtGt = Date.now()-3600000;
    let attempts = await db.LoginAttempt.sum('count', { where:{
        [Op.or]:[{ username }, { ip }],
        createdAt:{ [Op.gt]:createdAtGt }
    } });

    const res = await db.LoginAttempt.findCreateFind({
        where: { ip, username, createdAt:{ [Op.gt]:createdAtGt } },
        defaults: { ip, username, createdAt:Date.now() }
    });
    const attempt = res[0];
    const newlyCreated = res[1];
    if (!newlyCreated) {
        attempt.count++;
    }

    attempts++; // add this login attempt to sum
    
    if (attempts > config.APP.MAX_LOGIN_ATTEMPTS_PER_HOUR) {
        attempt.critical = true;
        await attempt.save();
        throw 'too many login attempts';
    }

    const user = await db.User.findOne({ where:{ username }});
    if (!user) {
        await attempt.save();
        throw 'unknown user';
    }
    
    if (!user.checkPassword(password)) {
        await attempt.save();
        throw 'wrong password';
    }

    await db.Session.create({ id:sessionId, userId:user.id });
    attempt.userId = user.id;
    await attempt.save();
    return user;
}

async function logout(user, sessionId) {
    if (!user) throw 'not logged in';
    if (!sessionId || sessionId.length !== 88) throw 'bad session id';

    await db.Session.logout(sessionId);
}

async function logoutAll(user) {
    if (!user) throw 'not logged in';

    await db.Session.destroy({ where:{ userId:user.id } });
}

module.exports = {
    checkPwdLength,
    login,
    logout,
    logoutAll,
    postponeExpires,
    register,
}