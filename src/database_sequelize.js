const { Sequelize, Model, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');

const SALT_ROUNDS = 10;

const config = require('../config');
const sequelize = new Sequelize(config.MYSQL.DATABASE, config.MYSQL.USER, config.MYSQL.PASS, {
    host: config.MYSQL.HOST,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});



class IpAddress extends Model {
}
IpAddress.init({
    ip: {
        type: DataTypes.STRING(36),
        unique: true,
        validate: { isIP:true }
    },
    country: DataTypes.STRING(2),
    banned: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}, {
    sequelize,
    modelName: 'ip'
});



class User extends Model {
    forBrowser() {
        return {
            uid: this.uid,
            username: this.username,
            displayname: this.displayname,
            public: this.public,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            activities: this.activities
        };
    }
    checkPassword(plainPassword) {
        return bcrypt.compareSync(plainPassword, this.password);
    }
}
User.init({
    uid: {
        type: DataTypes.UUID,
        unique: true,
        defaultValue: Sequelize.UUIDV4
    },
    username: {
        type: DataTypes.STRING(32),
        unique: true
    },
    displayname: {
        type: DataTypes.STRING(128),
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: DataTypes.STRING,
    public: {
        type: DataTypes.BOOLEAN,
        defaultValue: 0
    }
}, {
    sequelize,
    modelName: 'user',
    paranoid: true
})

User.beforeCreate('register', (model, options) => {
    if (!model.username) {
        if (model.displayname)
            model.username = model.displayname.toLowerCase()
    } else {
        if (!model.displayname)
            model.displayname = model.username
        model.username = model.username.toLowerCase()
    }
    model.password = bcrypt.hashSync(model.password, SALT_ROUNDS)
})

IpAddress.hasMany(User, { as:'usersRegistrated', foreignKey:{ name:'registrationIpId', allowNull:false } });
IpAddress.hasMany(User, { as:'usersLast', foreignKey:{ name:'lastIpId' } });



class IpAddressBan extends Model {
}
IpAddressBan.init({
    note: DataTypes.TEXT,
    unbanAt: DataTypes.DATE,
    undone: { // ip was successfully unbanned
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}, {
    sequelize,
    modelName: 'ipban'
});

IpAddressBan.beforeCreate('ban', async (model, options) => {
    await IpAddress.update({ banned:true }, { where:{ id:model.ipId } });
});

IpAddress.hasMany(IpAddressBan);
IpAddressBan.belongsTo(IpAddress);



class LoginAttempt extends Model {
}
LoginAttempt.init({
    ip: {
        type: DataTypes.STRING(36),
        allowNull: false,
        validate: { isIP:true }
    },
    username: {
        type: DataTypes.STRING(32),
        allowNull: false
    },
    count: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    },
    critical: { // attempt triggered too many login attempts warning
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}, {
    sequelize,
    modelName: 'loginattempt'
});
LoginAttempt.belongsTo(User);
User.hasMany(LoginAttempt);



class Session extends Model {
    async postponeExpires() {
        this.expires = Date.now() + config.APP.SESSION_INACTIVE_TIME;
        await this.save();
    }
    static async logout(sessionId) {
        const session = await this.findOne({ where:{ id:sessionId }});
        if (session) await session.destroy();
    }
    static async destroyExpired() {
        await this.destroy({ where:{ expires:{ [Op.lt]:Date.now() } } });
    }
}
Session.init({
    id: {
        type: DataTypes.STRING(88),
        primaryKey: true
    },
    expires: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: User,
            key: 'id'
        }
    }
}, {
    sequelize,
    modelName: 'session'
});

Session.belongsTo(User, { allowNull:false });
User.hasMany(Session);



class Activity extends Model {
}
Activity.init({
    uid: {
        type: DataTypes.UUID,
        unique: true,
        defaultValue: Sequelize.UUIDV4
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    type: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    public: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 0
    }
}, {
    sequelize,
    modelName: 'activity',
    paranoid: true
});

Activity.belongsTo(User);
User.hasMany(Activity);



class Entry extends Model {
}
Entry.init({
    uid: {
        type: DataTypes.UUID,
        unique: true,
        defaultValue: Sequelize.UUIDV4
    },
    started: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },
    length: { // in milliseconds
        type: DataTypes.INTEGER,
        allowNull: false,
        default: 0
    },
    public: {
        type: DataTypes.BOOLEAN,
        defaultValue: 0
    },
    comment: {
        type: DataTypes.TEXT
    }
}, {
    sequelize,
    modelName: 'entry',
    paranoid: true
});

Activity.hasMany(Entry);
Entry.belongsTo(Activity);



sequelize.sync({
    // force: true
}).then(async () => {
    console.log('Synced models to db...')


});

module.exports = {
    sequelize,

    Activity,
    Entry,
    IpAddress,
    IpAddressBan,
    LoginAttempt,
    User,
    Session,
};