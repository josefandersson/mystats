const forge = require('node-forge')
const crypto = require('crypto')

function generateSalt() {
    return crypto.randomBytes(32).toString('hex');
}

function saltPassword(salt, password) {
    let sha256_md = forge.md.sha256.create()
    let serverSalt = 'MyNameIsJeff'
    sha256_md.update(salt + password + serverSalt)
    return sha256_md.digest().toHex();
}

function translateFromBits(decimal, keys) {
    let data = {}
    for (let i = 0; i < keys.length; i++) {
        data[keys[i]] = decimal & Math.pow(2, i) > 0
    }
    return data
}

function translateToBits(data) {
    let decimal = 0
    for (let key of data) {

    }
}

function t(d) {
    while (d > 0) {

    }
}

function d2b(decimal) {
    let b = []
    for (let i = 0; 0 < decimal; i++) {
        decimal = decimal >> 1
        b[i] = decimal & 1 > 0
    }
    return b
}

module.exports = { generateSalt, saltPassword, translateFromBits, translateToBits }