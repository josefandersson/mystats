function colorDegree(type, started, ms=604800000) { // one week in ms
    if (type === 0 || started == null || !isFinite(started)) {
        return null
    } else if (type === 1) { // pos
        return 120 - (Math.min((Date.now() - started) / ms, 1) * 120)
    } else if (type === 2) { // neg
        return Math.min((Date.now() - started) / ms, 1) * 120
    } else {
        return null;
    }
}

function intervalString(interval) {
    if (!interval || interval == 0) return ''

    let m = []

    let delta = interval
    m[0] = Math.floor(delta / 31557600000)
    delta = delta % 31557600000

    m[1] = Math.floor(delta / 604800000)
    delta = delta % 604800000

    m[2] = Math.floor(delta / 86400000)
    delta = delta % 86400000

    m[3] = Math.floor(delta / 3600000)
    delta = delta % 3600000

    m[4] = Math.floor(delta / 60000)
    delta = delta % 60000

    m[5] = Math.floor(delta / 1000)
    delta = delta % 1000

    m[6] = delta

    let done = false
    for (let i = 0; i < 7; i++) {
        if (done) m[i] = 0
        else {
            if (m[i] !== 0) {
                done = true
                i++
            }
        }
    }

    let string = ''
    for (let i = 0; i < 2; i++) {
             if (0 < m[0]) { if (m[0] == 1) string =  '1 year '       ;else string =  `${m[0]} years `       ;m[0] = 0 }
        else if (0 < m[1]) { if (m[1] == 1) string += '1 week '       ;else string += `${m[1]} weeks `       ;m[1] = 0 }
        else if (0 < m[2]) { if (m[2] == 1) string += '1 day '        ;else string += `${m[2]} days `        ;m[2] = 0 }
        else if (0 < m[3]) { if (m[3] == 1) string += '1 hour '       ;else string += `${m[3]} hours `       ;m[3] = 0 }
        else if (0 < m[4]) { if (m[4] == 1) string += '1 minute '     ;else string += `${m[4]} minutes `     ;m[4] = 0 }
        else if (0 < m[5]) { if (m[5] == 1) string += '1 second '     ;else string += `${m[5]} seconds `     ;m[5] = 0 }
        else break
    }

    if (string.length === 0) {
        if (m[6] == 1) string = '1 millisecond'
        else string = `${m[6]} milliseconds`
    }

    return string.trim()
}

function sinceTimestampString(timestamp) {
    if (timestamp == null || !isFinite(timestamp) || timestamp === 0) return null

    let str = intervalString(Date.now() - timestamp)
    if (str) return str + ' ago'
    else     return 'Literally right now'
}

try {
    const table = document.getElementById('activities');
    _json.activities.forEach(activity => {
        const started = new Date(activity.entry.started);
        const el = document.createElement('tr');
        let clr = colorDegree(activity.type, started.getTime()); // style="${clr ? `color:hsl(${clr},var(--s),var(--l-color))` : ''}"
        el.innerHTML =
`<td>
    <a href="/activity/${activity.uid}">${activity.title}</a>
</td>
<td>${activity.type}</td>`;
if (activity.entry) {
    el.innerHTML += `
<td>${activity.entries.count}</td>
<td title="${started.toLocaleString()}">${sinceTimestampString(started.getTime())}</td>
<td>${intervalString(activity.entry.length)}</td>
<td>${activity.entry.comment}</td>`;
        } else {
            el.innerHTML += `
<td cellspan="4">No entries</td>`;
        }
        if (clr) {
            el.style.backgroundColor = `hsl(${clr},var(--s),var(--l))`;
            // el.style.color = `hsl(${clr},var(--s),var(--l-color)) !important`;
            el.classList.add('dark-color');
        }
        table.appendChild(el);
    });
} catch (error) {console.log(error)}