const elLoginContainer = document.getElementById('login_container')
const elRegisterContainer = document.getElementById('register_container')
const elLogin = document.querySelector('form.login')
const elRegister = document.querySelector('form.register')

const elGotoLogin = document.getElementById('goto_login')
const elGotoRegister = document.getElementById('goto_register')

function submitForm(url, form) {
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: form.method,
            body: new URLSearchParams(new FormData(form))
        }).then(r => r.json()).then(resolve).catch(reject)
    })
}

elLogin.addEventListener('submit', ev => {
    ev.preventDefault()
    submitForm('/api/login', elLogin).then(json => {
        if (json.success) window.location.href = '/'
        else alert(json.error)
    })
})

elRegister.addEventListener('submit', ev => {
    ev.preventDefault()
    submitForm('/api/register', elRegister).then(json => {
        if (json.success) window.location.reload()
        else alert(json.error)
    })
})

elGotoLogin.addEventListener('click', ev => {
    ev.preventDefault()
    history.replaceState(null, 'Login', '/login')
    elRegisterContainer.setAttribute('hidden', true)
    elLoginContainer.removeAttribute('hidden')
})

elGotoRegister.addEventListener('click', ev => {
    ev.preventDefault()
    history.replaceState(null, 'Register', '/register')
    elLoginContainer.setAttribute('hidden', true)
    elRegisterContainer.removeAttribute('hidden')
})
