function makeTable() {
    try {
        const table = id('entries');
        sela('tr.dynamic').forEach(el => el.remove());
        _json.activity.entries.forEach((entry, i) => {
            const el = cr('tr', { className:'dynamic' });
            const started = new Date(entry.started);
            el.innerHTML =
`<td>${i}</td>
<td class="nowrap">
    <a href="/entry/${entry.uid}">${entry.uid}</a>
</td>
<td title="${started.toLocaleString()}" class="nowrap">${sinceTimestampString(started.getTime())}</td>
<td class="nowrap">${intervalString(entry.length)}</td>
<td>${entry.comment}</td>`;
            if (_isOwner) {
                el.innerHTML += `
<td class="nowrap">
    <a href="/api/entry/${entry.uid}/edit">Edit</a>
</td>
<td class="nowrap">
    <a href="/api/entry/${entry.uid}/remove">Remove</a>
</td>`;
            }
            table.appendChild(el);
        });
    } catch (error) { console.warn(error); }
}
makeTable();



try {
    const d = new Date();
    id('entry_date').value = `${d.getFullYear()}-${zeroes(d.getMonth()+1)}-${zeroes(d.getDate())}`;
    id('entry_time').value = `${zeroes(d.getHours())}:${zeroes(d.getMinutes())}`;
    
    id('entry_form').addEventListener('submit', ev => {
        ev.preventDefault();
        const fd = new FormData(ev.target);
        ev.target.disabled = true;
        let json = {
            uid: fd.get('uid'),
            hours: fd.get('hours'),
            minutes: fd.get('minutes'),
            seconds: fd.get('seconds'),
            public: fd.get('public'),
            comment: fd.get('comment'),
        };
        try {
            if (fd.get('date')) {
                let d;
                if (fd.get('time')) d = new Date(`${fd.get('date')} ${fd.get('time')}`);
                else                d = new Date(fd.get('date'));
                json.millis = d.getTime();
            } else {
                json.time = fd.get('time');
            }
        } catch(e) {
            json.date = fd.get('date');
            json.time = fd.get('time');
        }
        fetch('/api/entry/add', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        }).then(r => r.json()).then(json => {
            if (json.success) {
                _json.activity.entries.unshift(json.entry);
                makeTable();
            } else console.warn(json);
            ev.target.reset();
            ev.target.disabled = false;
            const d = new Date();
            id('entry_date').value = `${d.getFullYear()}-${zeroes(d.getMonth()+1)}-${zeroes(d.getDate())}`;
            id('entry_time').value = `${zeroes(d.getHours())}:${zeroes(d.getMinutes())}`;
        }).catch(console.warn);
    });
} catch (e) {}


document.body.addEventListener('click', ev => {
    if (ev.target.tagName === 'A') {
        if (ev.target.innerText === 'Edit') {
            ev.preventDefault();
            const commentParent = ev.target.parentElement.parentElement.children[4];
            const newEl = document.createElement('textarea');
            newEl.value = commentParent.innerText;
            commentParent.innerText = '';
            commentParent.appendChild(newEl);
            ev.target.innerText = 'Save';
        } else if (ev.target.innerText === 'Save') {
            ev.preventDefault();
            const commentParent = ev.target.parentElement.parentElement.children[4];
            const comment = commentParent.children[0].value;
            commentParent.children[0].remove();
            commentParent.innerText = comment;
            ev.target.innerText = 'Edit';
            fetch(ev.target.href, {
                method:'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ comment })
            }).then(r => r.json()).then(json => {
                console.log('Got json:', json);
            }).catch(err => {
                console.warn(err);
            });
        } else if (ev.target.innerText === 'Remove') {
            ev.preventDefault();
            if (confirm('Are you sure you want to remove an entry?')) {
                fetch(ev.target.href, {
                    method:'POST'
                }).then(r => r.json()).then(json => {
                    if (json.success) {
                        _json.activity.entries.splice(+ev.target.parentElement.parentElement.children[0].innerText, 1);
                        ev.target.parentElement.parentElement.remove();
                    } else {
                        console.warn(json);
                    }
                }).catch(console.warn);
            }
        }
    }
});


id('toggleCharts').addEventListener('click', ev => {
    const charts = id('charts');
    if (charts.getAttribute('hidden')) {
        charts.removeAttribute('hidden');
        ev.target.innerText = 'Hide charts';
    } else {
        charts.setAttribute('hidden', true);
        Chart.select(null);
        ev.target.innerText = 'Show charts';
    }
});




const canvas = document.getElementById('canvas');
const settingsContainer = document.getElementById('chartsSettings');
const ctx = canvas.getContext('2d');
let currentChart;

class Chart {
    constructor() {
        canvas.hidden = false;

        this.settings = {
            reverse: { text:'Reversed', type:'checkbox', defaultValue:false },
            color: { text:'Color', type:'select', options:[
                { text:'Solid', value:0 },
                { text:'By length', value:1 },
                { text:'By comment (full)', value:2 },
                { text:'By comment (word)', value:3 }
            ]}
        };
    }

    init() {
        this.createSettings();
        this.calculate();
        this.draw();
    }

    createSettings() {
        Object.entries(this.settings).forEach(([key, opts]) => {
            const id = `chartSetting_${key}`;
            const div = cr('div', { className:'chartSetting' });
            let input, event = 'change';
            switch (opts.type) {
                case 'select':
                    input = cr('select', { id, value:opts.defaultValue });
                    opts.options.forEach(option => {
                        const el = cr('option', { value:option.value, innerText:option.text })
                        input.appendChild(el);
                    });
                    break;
                case 'checkbox':
                    input = cr('input', { id, type:opts.type, checked:opts.defaultValue });
                    break;
                case 'range':
                    input = cr('input', { id, type:opts.type, min:opts.min, max:opts.max, step:opts.step, value:opts.defaultValue });
                    event = 'input';
                    break;
                default:
                    input = cr('input', { id, type:opts.type, value:opts.defaultValue });
            }
            input.addEventListener(event, ev => {
                let value;
                switch (ev.target.type) {
                    case 'checkbox':
                        value = ev.target.checked; break;
                    case 'number':
                    case 'range':
                        value = ev.target.valueAsNumber; break;
                    default:
                        value = ev.target.value;
                }
                currentChart.onSettingChanged(key, value);
                this.calculate();
                this.draw();
            });
            const label = cr('label', { htmlFor:id, innerText:opts.text });
            div.appendChild(label);
            div.appendChild(input);
            settingsContainer.appendChild(div);
        });

        const now = new Date();
        now.setHours(23);
        now.setMinutes(59);
        now.setSeconds(59);
        now.setMilliseconds(999);

        this.absMax = now.getTime();
        this.toTime = this.absMax;
        
        const then = new Date(_json.activity.entries[_json.activity.entries.length-1].started);
        then.setHours(0);
        then.setMinutes(0);
        then.setSeconds(0);
        then.setMilliseconds(0);

        this.absMin = then.getTime();
        this.fromTime = this.absMin;
        this.absRange = this.absMax - this.absMin;

        const minDate = sel('#dateInterval > .min');
        const maxDate = sel('#dateInterval > .max');
        const fromDate = sel('#dateInterval > .bar > div.from');
        const toDate = sel('#dateInterval > .bar > div.to');
        const fromInp = sel('#dateInterval > .bar > input.from');
        const toInp = sel('#dateInterval > .bar > input.to');

        minDate.innerText = then.toLocaleDateString();
        maxDate.innerText = now.toLocaleDateString();

        fromInp.min = this.absMin;
        fromInp.max = this.absMax;
        fromInp.value = this.absMin;
        fromInp.step = 86400000;

        toInp.min = this.absMin;
        toInp.max = this.absMax;
        toInp.value = this.absMax;
        toInp.step = fromDate.step;
        
        const onInput = () => {
            if (toInp.valueAsNumber <= fromInp.valueAsNumber)
                fromInp.valueAsNumber = toInp.valueAsNumber - 86400000;
            const fromPart = (fromInp.valueAsNumber - this.absMin) / this.absRange * 100 + '%';
            const toPart = (toInp.valueAsNumber - this.absMin) / this.absRange * 100 + '%';
            toInp.style.background = `linear-gradient(to right,#bbc ${fromPart},#889 ${fromPart} ${toPart},#bbc ${toPart})`;
            fromDate.style.left = fromPart;
            toDate.style.left = toPart;
            fromDate.innerText = new Date(fromInp.valueAsNumber).toLocaleDateString();
            toDate.innerText = new Date(toInp.valueAsNumber).toLocaleDateString();
        };
        fromInp.oninput = onInput;
        toInp.oninput = onInput;

        const onChange = () => {
            this.fromTime = fromInp.valueAsNumber;
            this.toTime = toInp.valueAsNumber;
            this.calculate();
            this.draw();
        };
        fromInp.onchange = onChange;
        toInp.onchange = onChange;
        
        onInput();
        id('dateInterval').hidden = false;
    }

    onSettingChanged(key, value) { this.settings[key].value = value; }
    getSetting(key) { return this.settings[key].value != null ? this.settings[key].value : this.settings[key].defaultValue; }

    close() {
        [...settingsContainer.children].forEach(el => el.remove());
        canvas.hidden = true;
        currentChart = null;
        id('dateInterval').hidden = true;
    }

    calculate() {
        const entries = _json.activity.entries.concat();
        entries.forEach(entry => {
            entry.startedDate = new Date(entry.started);
            entry.startedTime = entry.startedDate.getTime();
        });
        this.entries = entries.filter(entry => entry.startedTime > this.fromTime && entry.startedTime < this.toTime);
        
        this.w = canvas.width = canvas.offsetWidth;
        this.h = canvas.height = Math.min(canvas.offsetWidth * .4, window.innerHeight * .9);

        this.first = this.entries[this.entries.length - 1].startedDate;
        this.last = this.entries[0].startedDate;
        this.firstMillis = this.first.getTime();
        this.lastMillis = this.last.getTime();
        this.interval = Math.abs(this.firstMillis - this.lastMillis);
    }

    update() {
        
    }

    draw() {
        ctx.clearRect(0, 0, this.w, this.h);
    }

    static getName() { return null; }

    static select(chart) {
        if (!chart) if (currentChart) return currentChart.close(); else return;
        if (currentChart)
            if (chart.getName() === currentChart.constructor.getName()) return;
            else currentChart.close();
        currentChart = new chart();
        currentChart.init();
    }
}



class ColoredBoxes extends Chart {
    constructor() {
        super();

        this.settings.boxWidth = { text:'Box width', type:'range', min:5, max:50, defaultValue:25 };
        this.settings.boxHeight = { text:'Box height', type:'range', min:5, max:50, defaultValue:25 };
        this.settings.boxPadding = { text:'Box padding', type:'range', min:0, max:10, defaultValue:1 };

        this.settings.color.options.push({ text:'By comment (split)', value:4 });
        this.settings.color.defaultValue = 4;

        this.colors = {};
    }

    init() { super.init(); }

    calculate() {
        super.calculate();

        if (this.getSetting('reverse'))
            this.entries.reverse();

        const boxHeight = this.getSetting('boxHeight');
        const boxWidth = this.getSetting('boxWidth');
        const boxPadding = this.getSetting('boxPadding');
        const boxHeightPadding = boxHeight + boxPadding * 2;
        const boxWidthPadding = boxWidth + boxPadding * 2;
        
        const boxesPerRow = Math.floor(this.w / boxWidthPadding);

        this.textHeight = 20;
        this.textAreaY = 0;

        this.activeColors = {};

        this.entries.forEach((entry, i) => {
            const boxX = i % boxesPerRow;
            const boxY = Math.floor(i / boxesPerRow);
            const x = boxX * boxWidthPadding;
            const y = boxY * boxHeightPadding;

            this.textAreaY = y + boxHeightPadding * 1.5;

            const comment = entry.comment;
                // .replace(/\/gif\//g, 'gif')
                // .replace(/\/b\//g, 'b')
                // .replace(/\/hc\//g, 'hc')
                // .replace(/\/s\//g, 's')
                // .replace(/->$/, '');
            
            entry.parts = [];
            let rows = comment.split('->').map(row => row.split(','));
            const rowHeight = boxHeight / rows.length;
            rows.forEach((row, indexY) => {
                const offsetY = indexY * rowHeight;
                const rowWidth = boxWidth / row.length;
                row.forEach((col, indexX) => {
                    const offsetX = indexX * rowWidth;
                    const key = col.trim().toLowerCase().split(' ')[0];
                    if (!this.activeColors[key]) {
                        if (this.colors[key]) {
                            this.activeColors[key] = { color:this.colors[key].color, count:1 };
                        } else {
                            const r = Math.random() * 255;
                            const g = Math.random() * 255;
                            const b = Math.random() * 255;
                            const color = `rgb(${r},${g},${b})`;
                            this.colors[key] = { color };
                            this.activeColors[key] = { color, count:1 };
                        }
                    } else {
                        this.activeColors[key].count++;
                    }
                    entry.parts.push({
                        color: this.colors[key].color,
                        x: x + offsetX + boxPadding,
                        y: y + offsetY + boxPadding,
                        w: rowWidth,
                        h: rowHeight
                    });
                });
            });
        });
        this.textAreaHeight = this.h - this.textAreaY;
    }

    draw() {
        super.draw();

        this.entries.forEach(entry => {
            entry.parts.forEach(part => {
                ctx.fillStyle = part.color;
                ctx.fillRect(part.x, part.y, part.w, part.h);
            });
        });

        ctx.textBaseline = 'middle';

        let x = 10, y = 0, widest = 0;
        Object.entries(this.activeColors).sort((a, b) => b[1].count - a[1].count).forEach(([key, val]) => {
            ctx.fillStyle = val.color;
            if (y + this.textHeight > this.textAreaHeight) {
                x += widest + 25;
                widest = 0;
                y = 0;
            }

            const text = `[${val.count}] ${key}`;

            widest = Math.max(widest, ctx.measureText(text).width);
            ctx.fillRect(x, y + this.textAreaY + 5, 10, 10);
            ctx.fillText(text, x + 15, y + this.textAreaY + 10);

            y += this.textHeight;
        });
    }

    static getName() { return 'Colored boxes'; }
}



class DayByDay extends Chart {
    constructor() {
        super();

        this.settings.boxWidth = { text:'Box width', type:'range', min:5, max:50, defaultValue:25 };
        this.settings.boxHeight = { text:'Box height', type:'range', min:5, max:50, defaultValue:25 };
        this.settings.boxPadding = { text:'Box padding', type:'range', min:0, max:10, defaultValue:1 };

        this.settings.color.options.push({ text:'By comment (split)', value:4 });
        this.settings.color.defaultValue = 4;

        this.colors = {};
    }

    init() { super.init(); }

    calculate() {
        super.calculate();

        const boxHeight = this.getSetting('boxHeight');
        const boxWidth = this.getSetting('boxWidth');
        const boxPadding = this.getSetting('boxPadding');
        const boxHeightPadding = boxHeight + boxPadding * 2;
        const boxWidthPadding = boxWidth + boxPadding * 2;
        
        const boxesPerRow = Math.floor(this.w / boxWidthPadding);

        this.numDays = Math.round(this.interval / 86400000);
        this.days = [];
        this.boxes = [];
        this.rowHeights = [];

        this.entries.forEach((entry, i) => {
            const dayIndex = Math.floor((entry.startedTime - this.fromTime) / 86400000);
            const rowIndex = Math.floor(i / boxesPerRow);

            if (!this.days[dayIndex]) this.days[dayIndex] = [{ entry, i }];
            else this.days[dayIndex].push({ entry, i });

            this.rowHeights[rowIndex] = Math.max(this.rowHeights[rowIndex] || 0, this.days[dayIndex].length);
        });

        this.activeColors = {};
        let day = -1; let y = 0;
        for (let row = 0; row < this.numDays / this.boxesPerRow; row++) {
            y += this.rowHeights[row] * this.boxHeight;
            for (let boxX = 0; boxX < this.boxesPerRow; boxX++) {
                day++;
                if (day >= this.numDays) break;
                const date = new Date(this.fromTime + (day + 1) * 864e5);
                if (day === 0 || date.getDate() === 1) {

                }
            }
        }
    }

    draw() {
        super.draw();


    }

    static getName() { return 'Day by day'; }
}



class DayByDay2 extends Chart {
    constructor() {
        super();

        this.boxPadding = 1;
        this.boxWidthDraw = 15;
        this.boxHeightDraw = 15;
        this.boxWidth = this.boxWidthDraw + this.boxPadding * 2;
        this.boxHeight = this.boxHeightDraw + this.boxPadding * 2;
        this.marginRow = 10;

        this.boxesPerRow = Math.floor(this.w / this.boxWidth);

        this.numDays = 451;//365; // chart last year
        this.endMillis = Date.now();
        this.startMillis = this.endMillis - (this.numDays * 86400000);

        const entries = _json.activity.entries.concat();
        this.days = [];

        const rowHeights = [];
        entries.forEach((entry, i) => {
            const startedMillis = new Date(entry.started).getTime();
            if (startedMillis < this.startMillis || this.endMillis < startedMillis) return;

            const day = Math.floor((startedMillis - this.startMillis) / 86400000);
            const row = Math.floor(day / this.boxesPerRow);
            if (!this.days[day]) this.days[day] = [{entry, i}];
            else this.days[day].push({entry, i});

            rowHeights[row] = Math.max(rowHeights[row] || 0, this.days[day].length);
        });

        this.colors = {};
        let day = -1, y = 0;
        for (let row = 0; row < this.numDays / this.boxesPerRow; row++) {
            y += rowHeights[row] * this.boxHeight;
            for (let boxX = 0; boxX < this.boxesPerRow; boxX++) {
                day++;
                if (day >= this.numDays) break;
                const date = new Date(this.startMillis + (day + 1) * 86400000);
                if (day === 0 || date.getDate() === 1) {
                    ctx.fillStyle = 'rgba(255,0,0,.5)';
                    ctx.fillRect(boxX * this.boxWidth, y - this.boxHeight * rowHeights[row], this.boxPadding, this.boxHeight * rowHeights[row] + this.marginRow / 2);
                    ctx.textBaseline = 'top';
                    ctx.fillText(date.toLocaleDateString(), boxX * this.boxWidth + this.boxPadding, y);
                }
                const objects = this.days[day];
                if (objects) {
                    objects.forEach((obj, i) => {
                        const rx = boxX * this.boxWidth + this.boxPadding;
                        const ry = y + -(i + 1) * this.boxHeight + this.boxPadding;

                        const entry = obj.entry;
                        const comment = entry.comment
                            .replace(/\/gif\//g, 'gif')
                            .replace(/\/b\//g, 'b')
                            .replace(/\/hc\//g, 'hc')
                            .replace(/\/s\//g, 's')
                            .replace(/->$/, '');
                        let rows = comment.split('->').map(row => row.split('/'));
                        const rowHeight = this.boxHeightDraw / rows.length;
                        rows.forEach((row, indexY) => {
                            const offsetY = indexY * rowHeight;
                            const rowWidth = this.boxWidthDraw / row.length;
                            row.forEach((col, indexX) => {
                                const offsetX = indexX * rowWidth;
                                const key = col.trim().toLowerCase().split(' ')[0];
                                if (!this.colors[key]) {
                                    const r = Math.random() * 255;
                                    const g = Math.random() * 255;
                                    const b = Math.random() * 255;
                                    this.colors[key] = { color:`rgb(${r},${g},${b})`, count:1 };
                                } else {
                                    this.colors[key].count++;
                                }
                                ctx.fillStyle = this.colors[key].color;
                                ctx.fillRect(rx + offsetX, ry + offsetY, rowWidth, rowHeight);
                            });
                        });
                    });
                } else {
                    ctx.fillStyle = `rgba(5,5,5,.05)`;
                    ctx.fillRect(boxX * this.boxWidth + this.boxPadding, y - this.boxHeight + this.boxPadding, this.boxWidthDraw, this.boxHeightDraw);
                }
            }
            y += this.marginRow;
        }

        ctx.textBaseline = 'middle';

        const textRowHeight = 20;
        const textAreaOffset = y;
        const textAreaHeight = this.h - textAreaOffset;

        let x = 10, widest = 0;
        y = 0;
        Object.entries(this.colors).sort((a, b) => b[1].count - a[1].count).forEach(([key, val]) => {
            ctx.fillStyle = val.color;
            if (y + textRowHeight > textAreaHeight) {
                x += widest + 25;
                widest = 0;
                y = 0;
            }

            const text = `[${val.count}] ${key}`;

            widest = Math.max(widest, ctx.measureText(text).width);
            ctx.fillRect(x, y + textAreaOffset + 5, 10, 10);
            ctx.fillText(text, x + 15, y + textAreaOffset + 10);

            y += textRowHeight;
        });

        console.log({ colors:this.colors, rowHeights });
    }
}



class StartAndLengthCandles extends Chart {
    constructor() {
        super();

        this.settings.lineWidth = { text:'Line width', type:'range', defaultValue:3, min:1, max:20 };
    }

    init() { super.init(); }
    calculate() {
        super.calculate();

        this.maxLength = 0;
        this.maxStarted;

        let total = 0, totalCount = 0;
        this.entries.forEach(entry => {
            if (!entry.length || entry.length === 0) return;
            if (this.maxLength < entry.length) {
                this.maxLength = entry.length;
                this.maxStarted = entry.started;
            }
            total += entry.length;
            totalCount ++;
        });
        this.exactAvgLength = total / totalCount;

        this.exactMidLength = this.maxLength / 2; // FIXME: (this.maxLength - this.minLength) / 2;
        this.midLength = 0;
        let midDistance = this.maxLength;
        this.midStarted;
        this.avgLength = 0;
        let avgDistance = this.maxLength;
        this.avgStarted;

        this.entries.forEach(entry => {
            if (!entry.length) return;
            let distToMid = Math.abs(entry.length - this.exactMidLength);
            if (distToMid < midDistance) {
                midDistance = distToMid;
                this.midLength = entry.length;
                this.midStarted = entry.started;
            }
            let distToAvg = Math.abs(entry.length - this.exactAvgLength);
            if (distToAvg < avgDistance) {
                avgDistance = distToAvg;
                this.avgLength = entry.length;
                this.avgStarted = entry.started;
            }
        });
    }

    draw() {
        super.draw();
        
        ctx.fillStyle = 'brown';
        ctx.textBaseline = 'bottom';
        ctx.textAlign = 'left';
        ctx.fillText(this.first.toLocaleDateString(), 5, 20);
        ctx.textAlign = 'right';
        ctx.fillText(this.last.toLocaleDateString(), this.w - 5, 20);
        ctx.textAlign = 'center';
        ctx.fillStyle = 'red';
        ctx.fillText(intervalString(this.maxLength) + ' (max)',
            (new Date(this.maxStarted).getTime() - this.firstMillis) / this.interval * (this.w - 13) + 5, 20);
        ctx.fillText(intervalString(this.midLength) + ' (mid)',
            (new Date(this.midStarted).getTime() - this.firstMillis) / this.interval * (this.w - 13) + 5, 20);
        ctx.fillText(intervalString(this.avgLength) + ' (avg)',
            (new Date(this.avgStarted).getTime() - this.firstMillis) / this.interval * (this.w - 13) + 5, 20);
        
        const w = this.getSetting('lineWidth');
        this.entries.forEach(entry => {
            const x = (new Date(entry.started).getTime() - this.firstMillis) / this.interval * (this.w - 13) + 5;
            let h = 3;
            if (entry.length) {
                // w = entry.length / interval * this.w * 5;
                h = entry.length / this.maxLength * (this.h - 20);
            }
            if (this.maxStarted === entry.started || this.midStarted === entry.started || this.avgStarted === entry.started)
                ctx.fillStyle = 'red';
            else 
                ctx.fillStyle = 'brown';
            ctx.fillRect(x, 20, w, h);
        });
    }

    static getName() { return 'Time and length candles'; }
}




class TimeDensity extends Chart {
    constructor() {
        super();

        this.settings.lineWidth = { text:'Line width', type:'number', defaultValue:15 };
    }

    init() {
        super.init();
    }

    calculate() {
        super.calculate();
    }

    draw() {
        super.draw();

        const reverse = !this.getSetting('reverse');
        let t0 = this.first.toLocaleDateString();
        let t1 = t0;
        if (reverse) t0 = this.last.toLocaleDateString();
        else         t1 = this.last.toLocaleDateString();

        ctx.fillStyle = 'rgb(100,150,180)';
        ctx.textBaseline = 'bottom';
        ctx.textAlign = 'left';
        ctx.fillText(t0, 5, 20);
        ctx.textAlign = 'right';
        ctx.fillText(t1, this.w - 5, 20);
        ctx.fillStyle = 'rgba(100,150,180,.2)';

        const lineWidth = this.getSetting('lineWidth');
        this.entries.forEach(entry => {
            let x = Math.round((new Date(entry.started).getTime() - this.firstMillis) / this.interval * (this.w - 13) + 5 - lineWidth / 2);
            if (reverse) x = (this.w - 5) - x;
            ctx.fillRect(x, 20, lineWidth, this.h);
        });
    }

    static getName() { return 'Time density'; }
}

Chart.CHARTS = [
    ColoredBoxes,
    DayByDay,
    StartAndLengthCandles,
    TimeDensity
];

const chartsButtons = id('chartsButtons');
Chart.CHARTS.forEach(chart => {
    const div = document.createElement('div');
    const radio = cr('input', { type:'radio', name:'chart', id:`chart_${chart.getName()}`, onchange:() => Chart.select(chart) });
    const label = cr('label', { htmlFor:radio.id, innerText:chart.getName() });
    div.appendChild(radio);
    div.appendChild(label);
    chartsButtons.appendChild(div);
});

let resizeTimeout;
window.addEventListener('resize', () => {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(() => {
        if (currentChart) {
            currentChart.calculate();
            currentChart.draw();
        }
    }, 100);
});

