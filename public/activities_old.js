const elAddActivity = document.getElementById('add_activity')

const elPopup = document.getElementById('popup')
const elPopupClose = document.getElementById('popup_close')
const elFormActivity = document.getElementById('activity_form')
const elFormEntry = document.getElementById('entry_form')
const elFormEdit = document.getElementById('edit_activity_form')
const elActivityId = document.getElementById('activity_id')

function openPopup(form) {
    elPopup.removeAttribute('hidden')
    form.removeAttribute('hidden')
}

function closePopup() {
    elPopup.setAttribute('hidden', true)
    elFormActivity.setAttribute('hidden', true)
    elFormEntry.setAttribute('hidden', true)
    elFormEdit.setAttribute('hidden', true)
}

function submitForm(form) {
    return new Promise((resolve, reject) => {
        fetch(form.action, {
            method: form.method,
            body: new URLSearchParams(new FormData(form))
        }).then(r => r.json()).then(resolve).catch(reject)
    })
}

elAddActivity.addEventListener('click', e => {
    openPopup(elFormActivity)
})

document.addEventListener('click', e => {
    if (e.target.classList.contains('add_entry')) {
        document.getElementById('add_activity_id').value = e.target.getAttribute('activity_id')
        let d = new Date()
        document.getElementById('inp4').value = d.getFullYear() + '-' + zeros(d.getMonth() + 1) + '-' + zeros(d.getDate())
        document.getElementById('inp5').value = zeros(d.getHours()) + ':' + zeros(d.getMinutes())
        openPopup(elFormEntry)
    } else if (e.target.classList.contains('edit_activity')) {
        let activity_id = e.target.getAttribute('activity_id')
        document.getElementById('edit_activity_id').value = activity_id
        document.getElementById('inp9').value = e.target.getAttribute('activity_title')
        document.getElementById('inp10').value = e.target.getAttribute('activity_type')
        document.getElementById('inp11').checked = e.target.getAttribute('activity_public') === '1'
        elFormEdit.action = `/api/activity/${activity_id}/edit`
        openPopup(elFormEdit)
    } else if (e.target.classList.contains('remove_activity')) {
        if (confirm('Do you really wish to remove the activity? (This action cannot be reverted)')) {
            fetch('/api/activity/remove', { method:'POST', body:new URLSearchParams('activity_id=' + e.target.getAttribute('activity_id')) }).then(r => r.json()).then(json => {
                console.log('Got response:', json)
                if (json.success) location.reload()
                else alert('Something went wrong!')
            }).catch(console.log)
        }
    }
})

elPopup.addEventListener('click', e => {
    if (e.target === elPopupClose) {
        closePopup()
    }
})

elFormActivity.addEventListener('submit', e => {
    e.preventDefault()
    submitForm(elFormActivity).then(json => {
        console.log('Got response:', json)
        if (json.success) location.reload()
        else alert('Something went wrong!')
    }).catch(console.log)
})

elFormEntry.addEventListener('submit', e => {
    e.preventDefault()
    submitForm(elFormEntry).then(json => {
        console.log('Got response:', json)
        if (json.success) location.reload()
        else alert('Something went wrong!')
    })
})

elFormEdit.addEventListener('submit', e => {
    e.preventDefault()
    submitForm(elFormEdit).then(json => {
        console.log('Got response:', json)
        if (json.success) location.reload()
        else alert('Something went wrong!')
    })
})

let measure
function textWidth(text) {
    if (!measure){
        measure = document.createElement('span')
        measure.style.position = 'fixed'
        measure.style.top = '-200px'
        measure.style.fontSize = '10px'
        document.body.appendChild(measure)
    } else {
        // measure.removeAttribute('hidden')
    }
    measure.innerText = text
    let width = measure.offsetWidth
    // measure.setAttribute('hidden', true)
    return width
}

// function resizeTexts() {
//     document.querySelectorAll('.left').forEach(el => {
//         let mw = el.offsetWidth - 130
//         let cw = textWidth(el.innerText)
//         let newSize = Math.floor(mw / cw * 10)
//         el.style.fontSize = Math.min(newSize, 30) + 'px'
//     })
//     document.querySelectorAll('.interval').forEach(el => {
//         let mw = el.parentElement.offsetWidth - 70
//         let cw = textWidth(el.innerText)
//         let newSize = Math.floor(mw / cw * 10)
//         el.style.fontSize = Math.min(newSize, 13) + 'px'
//     })
// }
// resizeTexts()

// window.addEventListener('resize', resizeTexts)

function zeros(val, amount=2) {
    let str = val + ''
    while (str.length < amount) {
        str = '0' + str
    }
    return str
}




[...document.getElementsByClassName('bar')].forEach(el => {
    let created = parseInt(el.getAttribute('created'))
    let timestamp = parseInt(el.getAttribute('timestamp'))
    if (created)
        el.querySelector('.created').title = new Date(created).toLocaleString()
    if (timestamp)
        el.querySelector('.since').title = new Date(timestamp).toLocaleString()
})