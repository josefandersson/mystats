function intervalString(interval, precision=2) {
    if (!interval || interval == 0) return '';

    const periods = [
        [31557600000, 'year'],
        [604800000, 'week'],
        [86400000, 'day'],
        [3600000, 'hour'],
        [60000, 'minute'],
        [1000, 'second']];

    let str = '';
    let delta = interval;
    let prev = false;
    for (let [period, name] of periods) {
        const num = Math.floor(delta / period);
        if (num > 0) {
            if (precision--) {
                str += `${num} ${name}`;
                if (num > 1) str += 's ';
                else str += ' ';
                prev = true;
            } else {
                break;
            }
        } else if (prev) {
            break;
        }
        delta %= period;
    }

    return str.trimRight();
}

function sinceTimestampString(timestamp) {
    if (timestamp == null || !isFinite(timestamp) || timestamp === 0) return '';

    let str = intervalString(Date.now() - timestamp);
    if (str) return str + ' ago';
    else     return 'Literally right now';
}

function zeroes(obj, len=2) {
    let str = String(obj);
    if (str.length >= len) return str;
    return '0'.repeat(len - str.length) + str;
}

function oneYearAgo() {
    const now = new Date();
    now.setFullYear(now.getFullYear() - 1);
    return now;
}

const id = id => document.getElementById(id);
const sel = selector => document.querySelector(selector);
const sela = selector => [...document.querySelectorAll(selector)];
const cr = (type, obj) => Object.assign(document.createElement(type), obj);

const html = document.head.parentElement;

let colorMode = localStorage.getItem('colorMode');
let browserPreference = window.getComputedStyle(html).content?.replace(/"/g, '') || 'light';

function updateColors() {
    if ((colorMode || browserPreference) === 'light') {
        html.classList.remove('dark');
        html.classList.add('light');
        return 'Dark mode';
    } else {
        html.classList.remove('light');
        html.classList.add('dark');
        return 'Light mode';
    }
}

updateColors();

function toggleColors() {
    if (colorMode == null || colorMode === browserPreference) {
        colorMode = browserPreference === 'light' ? 'dark' : 'light';
        localStorage.setItem('colorMode', colorMode);
    } else {
        colorMode = null;
        localStorage.removeItem('colorMode');
    }
    document.getElementById('colorModeToggle').innerText = updateColors();
}