# MyStats

MyStats is a webpage for tracking activities (eg. alcohol consumption, driving, workouts) and visualising the data, written in NodeJS.

This project started as a way to track the creator's energy drink consumption and has been [used by him for over a year](https://mystats.josefadventures.org/activity/ced76345-285c-42b4-a08e-7d4bed0f9205).

## Features

- Forced tls
- Homemade authentication/authorization system with bcrypt encryption
- Authorization throttling for users and IPs
- Uses MySQL through [sequelize](https://www.npmjs.com/package/sequelize) (both using the ORM and raw SQL queries)
- Light and dark theme

[Registration your own account here](https://mystats.josefadventures.org/register).
